#pragma once

#include "pros/misc.h"

#define LIFT_UP_BTN         DIGITAL_L1
#define LIFT_DOWN_BTN       DIGITAL_L2

#define FORK_OUT_BTN        DIGITAL_R1
#define FORK_IN_BTN         DIGITAL_R2

#define FLIP_DRIVE_BTN      DIGITAL_RIGHT

#define LIFT_HIGH_BTN       DIGITAL_UP
#define LIFT_MED_BTN        DIGITAL_LEFT
#define LIFT_LOW_BTN        DIGITAL_DOWN
