#pragma once

#include "pros/motors.hpp"
#include "okapi/api/coreProsAPI.hpp"
#include "okapi/impl/chassis/controller/chassisControllerFactory.hpp"
#include "okapi/impl/control/iterative/iterativeControllerFactory.hpp"
#include "okapi/api/units/QLength.hpp"
#include "okapi/api/units/QAngle.hpp"
#include "okapi/impl/util/timer.hpp"


#define LIFT_LOW_CONST  0
#define LIFT_MED_CONST  1
#define LIFT_HIGH_CONST 2

using pros::Motor;
using pros::Controller;
using okapi::QLength;
using namespace okapi::literals;

extern Controller master;
extern Motor drive_left_1;
extern Motor drive_left_2;
extern Motor drive_right_1;
extern Motor drive_right_2;

extern Motor lift_left_bottom;
extern Motor lift_left_top;
extern Motor lift_right_bottom;
extern Motor lift_right_top;

extern Motor fork_mtr;


/** Chassis Controller **/

extern std::shared_ptr<okapi::ChassisControllerPID> chassisController;

/** Lift PID Controller **/

extern okapi::IterativePosPIDController liftController;
extern okapi::IterativePosPIDController liftVertCorrectionController;
extern okapi::IterativePosPIDController liftSwayCorrectionController;

/** Fork PID Controller **/

extern okapi::IterativePosPIDController forkController;

/** Functions **/
extern int liftStartPointDegrees;
extern double liftAdjustments[];
extern int liftTarget;

extern int liftStartPointDegrees;
void setLiftStartPoint_degrees(QLength degrees);
double getLiftPosition_inches();
double getLiftPosition();
double getLiftPower();
double getLiftLeftPosition();
double getLiftRightPosition();
double getForkPosition();
double getSwayError();
double getVertError();
// void moveDrive(int leftPower, int rightPower, bool reverse = false);
// void moveDriveTimed(int leftPower, int rightPower, int time_ms, bool reverse = false);
void moveLiftLeft(int power);
void moveLiftRight(int power);
void stepLift();
void moveFork(int power);
void moveLift(int power);
void targetLift(int targetConstant, bool toggleDrop = false);
int currentTargetConstant();
void updateTarget();
void addLiftAdjustment(double adjustment);
void setLiftAdjustment(double adjustment);

void disableAllControllers();
void resetLiftMotors();

void toggleFork();
void stepFork();



/** MISC **/
