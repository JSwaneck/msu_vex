
#include "pros/motors.hpp"
#include "okapi/api/coreProsAPI.hpp"
#include "okapi/impl/chassis/controller/chassisControllerFactory.hpp"
#include "okapi/impl/control/iterative/iterativeControllerFactory.hpp"
#include "okapi/api/units/QLength.hpp"
#include "user_motor.hpp"

#define AUTON_NONE  -1
#define AUTON_RED    1
#define AUTON_BLUE   2
#define AUTON_SKILLS 3

extern int selectedAuton;
extern bool auton_far;
extern bool auton_delay_switch;
extern bool auton_floor;
extern bool op_control;

void set_auton(int auton);
