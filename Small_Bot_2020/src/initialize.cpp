#include "main.h"

/**
 * MSU VEX 2018-19
 * Jacob Swaneck
*/




void on_center_button() {
	static bool center_pressed = false;
	center_pressed = !center_pressed;
	if (center_pressed && op_control) {
		pros::delay(1000);
		autonomous();
	}
}

void on_left_button(){
	static bool left_pressed = false;
	left_pressed = !left_pressed;
	if(left_pressed){
		  set_auton(AUTON_RED);
	}
}

void on_right_button(){
	static bool right_pressed = false;
	right_pressed = !right_pressed;
	if(right_pressed){
		set_auton(AUTON_BLUE);
	}
}

/**
 * Runs initialization code. This occurs as soon as the program is started.
 *
 * All other competition modes are blocked by initialize; it is recommended
 * to keep execution time for this mode under a few seconds.
 */
void initialize() {
	pros::lcd::initialize();
	//pros::lcd::set_text(1, "Hello PROS User!");
	 pros::lcd::register_btn0_cb(on_left_button);
	 pros::lcd::register_btn1_cb(on_center_button);
	 pros::lcd::register_btn2_cb(on_right_button);
	//runScreen();
	liftController.setTarget(0);
	liftSwayCorrectionController.setTarget(0);
	liftVertCorrectionController.setTarget(0);
}

/**
 * Runs while the robot is in the disabled state of Field Management System or
 * the VEX Competition Switch, following either autonomous or opcontrol. When
 * the robot is enabled, this task will exit.
 */
void disabled() {}

/**
 * Runs after initialize(), and before autonomous when connected to the Field
 * Management System or the VEX Competition Switch. This is intended for
 * competition-specific initialization routines, such as an autonomous selector
 * on the LCD.
 *
 * This task will exit when the robot is enabled and autonomous or opcontrol
 * starts.
 */
#define LOOPS_PER_FRAME 50
void competition_initialize() {
		/*
		pros::lcd::print(5, "Welcome!!!");
		int loop_counter = 0;
		int frame = 0;

		while(1){

			if(auton_choice == AUTON_NONE){
				loop_counter++;
				if(loop_counter >= LOOPS_PER_FRAME){
					loop_counter = 0;
					frame++;
					if(frame >= 1)
						frame = 0;

					pros::lcd::clear();
					switch(frame){
						case 0:
							pros::lcd::print(5, "-------- CHOOSE AUTON --------");
							break;
						case 1:
							break;
					}
				}
			}
			else if(auton_choice == AUTON_RED){
				pros::lcd::print(5, "       Auton RED       ");
			}
			else{
					pros::lcd::print(5, "       Auton BLUE       ");
			}

			pros::delay(20);
		}*/



}
