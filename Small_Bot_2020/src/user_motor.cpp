#include "main.h"

/**
 * MSU VEX 2018-19
 * Jacob Swaneck
*/

#define LIFT_LOW_TARGET   0
#define LIFT_MED_TARGET   1200
#define LIFT_HIGH_TARGET  1600

// #define LIFT_DROP         300

#define FORK_STEP 100

/** PID **/
#define NEGATIVE_SCALE 0.5

#define LIFT_KP 0.0015
#define LIFT_KI 0.05 //0.015
#define LIFT_KD 0.01 //2.0

#define LIFT_SLEW 20

#define LIFT_SWAY_TOP_CORRECTION 0.1

#define LIFT_SWAY_KP 0.005
#define LIFT_SWAY_KI 0.2
#define LIFT_SWAY_KD 0.07

#define LIFT_VERT_KP 0.002
#define LIFT_VERT_KI 0.0002
#define LIFT_VERT_KD 0.001

/** DRIVE PID **/

#define DISTANCE_KP   0.0008
#define DISTANCE_KI   0 //0.0005
#define DISTANCE_KD   0 //0.06

#define ANGLE_KP      0.0007
#define ANGLE_KI      0.000
#define ANGLE_KD      0.00

#define TURN_KP       0.0020
#define TURN_KI       0 //0.00003
#define TURN_KD       0 //2.0

#define FORK_KP       0.01
#define FORK_KI       0.00
#define FORK_KD       0.00


/** Motor Definitions **/
Controller master(pros::E_CONTROLLER_MASTER);

Motor lift_left_top = Motor(10);
Motor lift_left_bottom = Motor(9, true);
Motor lift_right_top = Motor(7, true);
Motor lift_right_bottom = Motor(8);

Motor fork_mtr = Motor(6, true);

/// IF REVERSING DRIVE MOTORS, CHANGE CHASSIS CONTROLLER BELOW
Motor drive_right_1 = Motor(3, true);
Motor drive_right_2 = Motor(4, true);
Motor drive_left_1 = Motor(1);
Motor drive_left_2 = Motor(2);

/** Chassis Controller **/

auto distanceGains = okapi::IterativePosPIDController::Gains{DISTANCE_KP, DISTANCE_KI, DISTANCE_KD};
auto angleGains = okapi::IterativePosPIDController::Gains{ANGLE_KP, ANGLE_KI, ANGLE_KD};
auto turnGains = okapi::IterativePosPIDController::Gains{TURN_KP, TURN_KI, TURN_KD};

std::shared_ptr<okapi::ChassisControllerPID> chassisController = okapi::ChassisControllerFactory::createPtr(
  {1, 2}, //{-14, 15, 16},
  {-3, -4}, //{11, -12, -13},
  distanceGains,
  angleGains,
  turnGains,
  okapi::AbstractMotor::gearset::red,
  {2.5_in , 11.75_in}
);

/** Lift PID Controller **/

okapi::IterativePosPIDController liftController = okapi::IterativeControllerFactory::posPID(LIFT_KP,LIFT_KI,LIFT_KD);
okapi::IterativePosPIDController liftVertCorrectionController = okapi::IterativeControllerFactory::posPID(LIFT_VERT_KP,LIFT_VERT_KI,LIFT_VERT_KD);
okapi::IterativePosPIDController liftSwayCorrectionController = okapi::IterativeControllerFactory::posPID(LIFT_SWAY_KP,LIFT_SWAY_KI,LIFT_SWAY_KD);

/** Fork PID Controller **/

okapi::IterativePosPIDController forkController = okapi::IterativeControllerFactory::posPID(FORK_KP, FORK_KI, FORK_KD);

/** Helper Functions **/

int liftStartPointDegrees;

void disableAllControllers(){
      liftController.flipDisable(true);
}

int liftTarget = 0;
bool drop = false;
double liftAdjustments[] = {0,0,0};

double getLiftPosition(){
  return (lift_left_bottom.get_position() +
          lift_right_bottom.get_position() +
          lift_left_top.get_position() +
          lift_right_top.get_position()
        ) / 4;
}

double getLiftLeftPosition(){
  return lift_left_bottom.get_position();
}

double getLiftRightPosition(){
  return lift_right_bottom.get_position();
}

double getForkPosition(){
  return fork_mtr.get_position();
}

double getSwayError(){
  // return (lift_left_bottom.get_position() + lift_left_top.get_position()) -
         // (lift_right_bottom.get_position() + lift_right_top.get_position());
  return (lift_left_bottom.get_position() + LIFT_SWAY_TOP_CORRECTION * lift_left_top.get_position()) -
         (lift_right_bottom.get_position() + LIFT_SWAY_TOP_CORRECTION * lift_right_top.get_position());
}

double getVertError(){
  return (lift_left_top.get_position() + lift_right_top.get_position()) -
         (lift_left_bottom.get_position() + lift_right_bottom.get_position());
}

double getLiftPosition_inches(){
    throw std::logic_error("Function not implemented");
}

void setLiftStartPoint_degrees(QLength degrees){
    throw std::logic_error("Function not implemented");
}

double liftpower = 0;
void stepLift(){
  double position = getLiftPosition();
  double swayError = getSwayError(); // Positive if left above right
  double vertError = getVertError(); // Positive if top ahead of bottom
  liftpower = liftController.step(position) * 127;
  double swayPower = liftSwayCorrectionController.step(swayError) * 127;
  double vertPower = liftVertCorrectionController.step(vertError) * 127;

  if(liftpower < 0){
    liftpower *= NEGATIVE_SCALE;
  }

  // pros::lcd::print(0, "Sway Power: %f", swayPower);
  // pros::lcd::print(1, "Vert Power: %f", vertPower);


  lift_left_top.move(liftpower + swayPower * LIFT_SWAY_TOP_CORRECTION + vertPower);
  lift_left_bottom.move(liftpower + swayPower - vertPower);
  lift_right_top.move(liftpower - swayPower * LIFT_SWAY_TOP_CORRECTION + vertPower);
  lift_right_bottom.move(liftpower - swayPower - vertPower);
}

double getLiftPower(){
  return liftpower;
}

int currentTargetConstant(){
  switch(liftTarget){
    case LIFT_LOW_TARGET:
      return LIFT_LOW_CONST;
    case LIFT_MED_TARGET:
      return LIFT_MED_CONST;
    case LIFT_HIGH_TARGET:
      return LIFT_HIGH_CONST;
    default:
      return -1;
  }
}

void addLiftAdjustment(double adjustment){
  int adjustmentIndex = currentTargetConstant();
  if(adjustmentIndex >= 0 && adjustmentIndex < 3){
      liftAdjustments[adjustmentIndex] += adjustment;
  }
  updateTarget();
}

void setLiftAdjustment(double adjustment){
  int adjustmentIndex = currentTargetConstant();
  if(adjustmentIndex >= 0 && adjustmentIndex < 3){
      liftAdjustments[adjustmentIndex] = adjustment;
  }
  updateTarget();
}

void targetLift(int targetConstant, bool toggleDrop){
    int target = 0;
    switch(targetConstant){
      case LIFT_LOW_CONST:
        target = LIFT_LOW_TARGET;
        break;
      case LIFT_MED_CONST:
        target = LIFT_MED_TARGET;
        break;
      case LIFT_HIGH_CONST:
        target = LIFT_HIGH_TARGET;
        break;
      default:
        return;
    }
    liftTarget = target;
    updateTarget();
}

void updateTarget(){
    int target = liftTarget;
    int targetConst = currentTargetConstant();
    if(targetConst != -1){
      target += liftAdjustments[targetConst];
    }
    liftController.setTarget(target);
}

void moveLift(int power){
    moveLiftLeft(power);
    moveLiftRight(power);
}

int liftLeftPower = 0;
void moveLiftLeft(int power){
    if(power > liftLeftPower + LIFT_SLEW)
      liftLeftPower += LIFT_SLEW;
    else if(power < liftLeftPower - LIFT_SLEW)
      liftLeftPower -= LIFT_SLEW;
    else
      liftLeftPower = power;

    // lift_left_bottom.move(liftLeftPower);
    lift_left_top.move(liftLeftPower);
}

int liftRightPower = 0;
void moveLiftRight(int power){
    if(power > liftRightPower + LIFT_SLEW)
      liftRightPower += LIFT_SLEW;
    else if(power < liftRightPower - LIFT_SLEW)
      liftRightPower -= LIFT_SLEW;
    else
      liftRightPower = power;

    // lift_right_bottom.move(liftRightPower);
    lift_right_top.move(liftRightPower);
}

void moveFork(int power){
  fork_mtr.move(power);
}

void resetLiftMotors(){
    lift_left_bottom.tare_position();
    lift_left_top.tare_position();
    lift_right_top.tare_position();
    lift_right_bottom.tare_position();

    double liftAdjustments[] = {0,0,0};
    liftTarget = 0;
    drop = false;
}

void toggleFork(){
  if(forkController.getTarget() == 0){
      forkController.setTarget(FORK_STEP);
  }
  else{
      forkController.setTarget(0);
  }
}

void stepFork(){
  double position = fork_mtr.get_position();
  double power = forkController.step(position) * 127;
  fork_mtr.move(power);
}
