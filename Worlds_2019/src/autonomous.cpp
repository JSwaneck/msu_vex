#include "main.h"

void auton_delay(uint32_t time){
  uint32_t start = pros::millis();
  pros::Task::delay_until(&start, time);
}

// void auton_test(){
//   moveFlywheel(100);
//   auton_delay(5000);
//   moveFlywheel(0);
// }

void auton_worlds(){
  //Initialize timer
  okapi::Timer timer = okapi::Timer();
  okapi::QTime time_start = timer.millis();

  //Settings for auton
  okapi::QAngle firstRotation, secondRotation, thirdRotation, floorRotation;
  int firstShotTarget, firstShotMin;
  int secondShotTarget, secondShotMin;
  if(auton_far){  // FAR AUTON
    firstRotation = 70_deg; //IGNORE
    secondRotation = 30_deg;//IGNORE
    thirdRotation = -50_deg;//IGNORE
    floorRotation = 70_deg;//IGNORE
    firstShotTarget = 2950;//IGNORE
    firstShotMin = 80;//IGNORE
    secondShotTarget = 3700;//IGNORE
    secondShotMin = 100;//IGNORE
  } else {        // MIDDLE AUTON                     VARIABLE HERE
    firstRotation = 85_deg;
    secondRotation = 20_deg;
    thirdRotation = -54_deg;
    floorRotation = 70_deg;
    firstShotTarget = 2950;
    firstShotMin = 80;
    secondShotTarget = 3700;
    secondShotMin = 100;
  }

  if(selectedAuton == AUTON_RED){
    firstRotation = -firstRotation;
    secondRotation = -secondRotation;
    thirdRotation = -thirdRotation;
    floorRotation = -floorRotation;
  }

  //BEGIN

  if(auton_delay_switch){
    auton_delay(10000);
  }

  // 1 - Pick up ball
  intake_mtr.move(127);
  chassisController->moveDistance(20_in);
  chassisController->stop();
  auton_delay(1000);
  chassisController->moveDistance(-2_in);
  chassisController->stop();
  intake_mtr.move(0);

  //2 - Rotate to shoot flag
  chassisController->turnAngle(firstRotation);
  chassisController->stop();
  auton_delay(500);

  // 3 - Start flywheel
  setFlywheelMin(firstShotMin);
  setFlywheelTarget(firstShotTarget);
  time_start = timer.millis();
  while(timer.millis() - time_start < 4000_ms){
    chassisController->stop();
    stepFlywheel();
    auton_delay(20);
  }

  // 4 - Shoot first flag (middle)
  time_start = timer.millis();
  while(timer.millis() - time_start < 1000_ms){
    chassisController->stop();
    stepFlywheel();
    moveConveyor(100);
    auton_delay(20);
  }
  moveConveyor(0);

  // 5 - Increase flywheel
  setFlywheelMin(secondShotMin);
  setFlywheelTarget(secondShotTarget);
  time_start = timer.millis();
  while(timer.millis() - time_start < 3000_ms){
    chassisController->stop();
    stepFlywheel();
    moveConveyor(0);
    auton_delay(20);
  }

  // 6 - Shoot second flag (high)
  intake_mtr.move(100);
  time_start = timer.millis();
  while(timer.millis() - time_start < 3000_ms){
    stepFlywheel();
    moveConveyor(100);
    auton_delay(20);
  }
  moveConveyor(0);
  intake_mtr.move(0);

  // 7 - Stop flywheel
  moveFlywheel(0);

  // 8 - Rotate towards cap
  chassisController->turnAngle(secondRotation);
  chassisController->turnAngle(0);
  chassisController->stop();
  auton_delay(1000);

  // 9 - Pick up cap
  chassisController->moveDistance(-8_in);
  chassisController->moveDistance(0);
  chassisController->stop();

  // 10 - Lift cap low
  targetLift(LIFT_LOW_CONST, false);
  targetLift(LIFT_LOW_CONST, true);
  time_start = timer.millis();
  while(timer.millis() - time_start < 1500_ms){
    stepLift();
    auton_delay(20);
  }
  moveLift(0);

  // 11 - Move back
  chassisController->moveDistance(7_in);
  chassisController->moveDistance(0);
  chassisController->stop();


  if(auton_floor){
    // 12 -  Flip cap
    time_start = timer.millis();
    toggleFork();
    while(timer.millis() - time_start < 2000_ms){
      stepFork();
      auton_delay(20);
    }
    fork_mtr.move(0);

    //13 - Rotate floor
    chassisController->turnAngle(floorRotation);
    chassisController->turnAngle(0);
    chassisController->stop();

    //14 - Lower lift
    targetLift(LIFT_LOW_CONST, false);
    time_start = timer.millis();
    while(timer.millis() - time_start < 1500_ms){
      stepLift();
      auton_delay(20);
    }
    moveLift(0);

    //15 - Drive back
    chassisController->moveDistance(15_in);
    chassisController->moveDistance(0);
    chassisController->stop();

  }
  else{
    // 12 - Lift cap med and flip
    time_start = timer.millis();
    toggleFork();
    targetLift(LIFT_MED_CONST, false);
    while(timer.millis() - time_start < 3000_ms){
      stepLift();
      stepFork();
      auton_delay(20);
    }
    moveLift(0);
    fork_mtr.move(0);

    //13 - Rotate to pole
    chassisController->turnAngle(thirdRotation);
    chassisController->turnAngle(0);
    chassisController->stop();
    auton_delay(1000);

    //14 - Drive to pole
    chassisController->moveDistance(12_in);
    chassisController->turnAngle(0);
    chassisController->stop();
  }
}

void auton_forward(){
      //Initialize timer
      okapi::Timer timer = okapi::Timer();
      okapi::QTime time_start = timer.millis();

      pros::lcd::print(7, "AUTON RED");

      //Start Flywheel
      moveFlywheel(127);
      //Lower fork
      fork_mtr.move(-80);
      pros::delay(2500);
      fork_mtr.move(0);

      auton_delay(2500);

      //Drive forward
      chassisController->moveDistance(30_in);

      //Shoot high flag
      time_start = timer.millis();
      intake_mtr.move(127);
      while(timer.millis() - time_start < 5000_ms){
        moveConveyor(100);
        auton_delay(20);
      }
      moveConveyor(0);

      //Drive forward
      chassisController->moveDistance(40_in);

      //Shoot mid flag
      time_start = timer.millis();
      while(timer.millis() - time_start < 5000_ms){
        moveConveyor(127);
        auton_delay(20);
      }
      moveConveyor(0);


      intake_mtr.move(0);
      moveFlywheel(0);
}

void autonomous() {
  
  ///// Settings /////
  chassisController->setMaxVoltage(80);
  chassisController->setMaxVelocity(150);
  bool was_op_control = op_control;
  op_control = false;
  pros::delay(100);
  ////////////////////

  ////// Auton ///////
  auton_worlds();

  ///// Settings /////
  setFlywheelMin(0);
  setFlywheelTarget(0);
  chassisController->setMaxVoltage(127);
  chassisController->setMaxVelocity(1000000);
  op_control = was_op_control;
  auton_delay(100);
  ////////////////////
}
