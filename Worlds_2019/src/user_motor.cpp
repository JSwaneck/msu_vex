#include "main.h"

/**
 * MSU VEX 2018-19
 * Jacob Swaneck
*/

#define LIFT_LOW_TARGET   0
#define LIFT_MED_TARGET   1550
#define LIFT_HIGH_TARGET  2200

#define LIFT_DROP         300

#define FLY_IDLE   0
#define FLY_MID_TARGET  2950
#define FLY_MID_MIN     80
#define FLY_HIGH_TARGET 3700
#define FLY_HIGH_MIN     100
#define FLY_THRESHOLD 10

#define FORK_STEP 2250

/** PID **/

#define LIFT_KP 0.0018
#define LIFT_KI 0.015
#define LIFT_KD 2.0

#define LIFT_SLEW 20

#define LIFT_COR_KP 0.0000
#define LIFT_COR_KI 0.0000
#define LIFT_COR_KD 0.0000

#define CON_KP  0.001
#define CON_KI  0.02
#define CON_KD  0.00
#define CON_MIN 10
#define CON_MAX 20

#define FLY_KP  0.020
#define FLY_KD  0.01

/** DRIVE PID **/

#define DISTANCE_KP   0.0008
#define DISTANCE_KI   0.0005
#define DISTANCE_KD   0.06

#define ANGLE_KP      0.0007
#define ANGLE_KI      0.000
#define ANGLE_KD      0.00

#define TURN_KP       0.0020
#define TURN_KI       0.00003
#define TURN_KD       2.0

#define FORK_KP       0.004
#define FORK_KI       0.00
#define FORK_KD       0.00


/** Motor Definitions **/
Controller master(pros::E_CONTROLLER_MASTER);

Motor lift_left_top = Motor(2, true);
Motor lift_left_bottom = Motor(3, true);
Motor lift_right_top = Motor(4);
Motor lift_right_bottom = Motor(5);

Motor conveyor_mtr_1 = Motor(8, true);
Motor conveyor_mtr_2 = Motor(9);
Motor fork_mtr = Motor(10, true);

/// IF REVERSING DRIVE MOTORS, CHANGE CHASSIS CONTROLLER BELOW
Motor drive_right_1 = Motor(11);
Motor drive_right_2 = Motor(12, true);
Motor drive_right_3 = Motor(13, true);
Motor drive_left_1 = Motor(14, true);
Motor drive_left_2 = Motor(15);
Motor drive_left_3 = Motor(16);

Motor intake_mtr = Motor(17, true);

Motor flywheel_1_mtr = Motor(18, pros::motor_gearset_e::E_MOTOR_GEARSET_06);
Motor flywheel_2_mtr = Motor(19, pros::motor_gearset_e::E_MOTOR_GEARSET_06, true);
Motor flywheel_3_mtr = Motor(20, pros::motor_gearset_e::E_MOTOR_GEARSET_06, true);



/** Chassis Controller **/

auto distanceGains = okapi::IterativePosPIDController::Gains{DISTANCE_KP, DISTANCE_KI, DISTANCE_KD};
auto angleGains = okapi::IterativePosPIDController::Gains{ANGLE_KP, ANGLE_KI, ANGLE_KD};
auto turnGains = okapi::IterativePosPIDController::Gains{TURN_KP, TURN_KI, TURN_KD};

std::shared_ptr<okapi::ChassisControllerPID> chassisController = okapi::ChassisControllerFactory::createPtr(
  {-14, 15}, //{-14, 15, 16},
  {11, -12}, //{11, -12, -13},
  distanceGains,
  angleGains,
  turnGains,
  okapi::AbstractMotor::gearset::red,
  {4_in , 15.4_in}
);

/** Lift PID Controller **/

okapi::IterativePosPIDController liftController = okapi::IterativeControllerFactory::posPID(LIFT_KP,LIFT_KI,LIFT_KD);
okapi::IterativePosPIDController liftCorrectionController = okapi::IterativeControllerFactory::posPID(LIFT_COR_KP,LIFT_COR_KI,LIFT_COR_KD);

/** Conveyor Correction Controller **/

okapi::IterativePosPIDController conveyorCorrectionController = okapi::IterativeControllerFactory::posPID(CON_KP, CON_KI, CON_KD);

/** Launcher PID Controller **/

okapi::IterativeVelPIDController flywheelController = okapi::IterativeControllerFactory::velPID(FLY_KP, FLY_KD);

/** Fork PID Controller **/

okapi::IterativePosPIDController forkController = okapi::IterativeControllerFactory::posPID(FORK_KP, FORK_KI, FORK_KD);

/** Helper Functions **/

int liftStartPointDegrees;

void disableAllControllers(){
      liftController.flipDisable(true);
      flywheelController.flipDisable(true);
}

int liftTarget = 0;
bool drop = false;
double liftAdjustments[] = {0,0,0};

double getLiftPosition(){
  return (lift_left_bottom.get_position() + lift_right_bottom.get_position()) / 2;
}

double getLiftLeftPosition(){
  return lift_left_bottom.get_position();
}

double getLiftRightPosition(){
  return lift_right_bottom.get_position();
}

double getLiftPosition_inches(){
    throw std::logic_error("Function not implemented");
}

void setLiftStartPoint_degrees(QLength degrees){
    throw std::logic_error("Function not implemented");
}

double liftpower = 0;
void stepLift(){
  double position = getLiftPosition();
  liftpower = liftController.step(position) * 127;
  moveLift(liftpower);
}

double getLiftPower(){
  return liftpower;
}

int currentTargetConstant(){
  switch(liftTarget){
    case LIFT_LOW_TARGET:
      return LIFT_LOW_CONST;
    case LIFT_MED_TARGET:
      return LIFT_MED_CONST;
    case LIFT_HIGH_TARGET:
      return LIFT_HIGH_CONST;
    default:
      return -1;
  }
}

void addLiftAdjustment(double adjustment){
  int adjustmentIndex = currentTargetConstant();
  if(adjustmentIndex >= 0 && adjustmentIndex < 3){
      liftAdjustments[adjustmentIndex] += adjustment;
  }
  updateTarget();
}

void setLiftAdjustment(double adjustment){
  int adjustmentIndex = currentTargetConstant();
  if(adjustmentIndex >= 0 && adjustmentIndex < 3){
      liftAdjustments[adjustmentIndex] = adjustment;
  }
  updateTarget();
}

void targetLift(int targetConstant, bool toggleDrop){
    int target = 0;
    switch(targetConstant){
      case LIFT_LOW_CONST:
        target = LIFT_LOW_TARGET;
        break;
      case LIFT_MED_CONST:
        target = LIFT_MED_TARGET;
        break;
      case LIFT_HIGH_CONST:
        target = LIFT_HIGH_TARGET;
        break;
      default:
        return;
    }
    drop = toggleDrop && target == liftTarget && !drop;
    liftTarget = target;
    updateTarget();
}

void updateTarget(){
    int target = liftTarget;
    if(drop){
      if(liftTarget == LIFT_LOW_TARGET)
        target += LIFT_DROP;
      else
        target -= LIFT_DROP;
    }
    int targetConst = currentTargetConstant();
    if(targetConst != -1){
      target += liftAdjustments[targetConst];
    }
    liftController.setTarget(target);
}

void moveLift(int power){
    moveLiftLeft(power);
    moveLiftRight(power);
}

int liftLeftPower = 0;
void moveLiftLeft(int power){
    if(power > liftLeftPower + LIFT_SLEW)
      liftLeftPower += LIFT_SLEW;
    else if(power < liftLeftPower - LIFT_SLEW)
      liftLeftPower -= LIFT_SLEW;
    else
      liftLeftPower = power;

    lift_left_bottom.move(liftLeftPower);
    lift_left_top.move(liftLeftPower);
}

int liftRightPower = 0;
void moveLiftRight(int power){
    if(power > liftRightPower + LIFT_SLEW)
      liftRightPower += LIFT_SLEW;
    else if(power < liftRightPower - LIFT_SLEW)
      liftRightPower -= LIFT_SLEW;
    else
      liftRightPower = power;

    lift_right_bottom.move(liftRightPower);
    lift_right_top.move(liftRightPower);
}


void resetLiftMotors(){
    lift_left_bottom.tare_position();
    lift_left_top.tare_position();
    lift_right_top.tare_position();
    lift_right_bottom.tare_position();

    double liftAdjustments[] = {0,0,0};
    liftTarget = 0;
    drop = false;
}

void enableConveyorPID(){
    conveyorCorrectionController.flipDisable(false);
    conveyorCorrectionController.setTarget(0);
}

void moveConveyor(int power){
    double correctivePower = 0;

    double pos_1 = conveyor_mtr_1.get_position();
    double pos_2 = conveyor_mtr_2.get_position();

    double diff = pos_2 - pos_1;

    if(!conveyorCorrectionController.isDisabled()){
        correctivePower = conveyorCorrectionController.step(diff) * 127;
        pros::lcd::print(5, "Corrective: %f", correctivePower);
        if(abs(correctivePower) < CON_MIN && power == 0)
          correctivePower = 0; //Prevents running the motors when they cant move
        else if(correctivePower < -CON_MAX)
          correctivePower = -CON_MAX;
        else if(correctivePower > CON_MAX)
          correctivePower = CON_MAX;
    }

    double pow_1 = fmin(127, power - correctivePower);
    double pow_2 = fmin(127, power + correctivePower);

    conveyor_mtr_1.move(pow_1);
    conveyor_mtr_2.move(pow_2);
}

void enableFlywheelPID(){
  flywheelController.setTicksPerRev(33.33);
}

double getFlywheelPosition(){
  return flywheel_2_mtr.get_position();
}

double getFlywheelVelocity(){
  return flywheel_2_mtr.get_actual_velocity() * 9;
}

void moveFlywheel(int power){
  flywheel_1_mtr.move(power);
  flywheel_2_mtr.move(power);
  flywheel_3_mtr.move(power);
}

void idleFlywheel(){
    setFlywheelTarget(FLY_IDLE);
}

void spinFlywheel(){
  setFlywheelTarget(FLY_HIGH_TARGET);
}

bool flywheelReady(){
  return flywheelController.getTarget() != FLY_IDLE
      && flywheelController.getError() < FLY_THRESHOLD;
}

double flyTarget = 0;
double flyMin = 0;
void toggleFlywheel(){
  if(flyTarget == FLY_IDLE){
    flyTarget = FLY_MID_TARGET;
    flyMin = FLY_MID_MIN;
  }
  else if(flywheelController.getTarget() == FLY_MID_TARGET){
    flyTarget = FLY_HIGH_TARGET;
    flyMin = FLY_HIGH_MIN;
  }
  else{
    flyTarget = FLY_IDLE;
    flyMin = 0;
  }

  flywheelController.setTarget(flyTarget);
};

void setFlywheelTarget(int target){
  flyTarget = target;
  flywheelController.setTarget(flyTarget);
}
void setFlywheelMin(int min){
  flyMin = min;
}
double flywheelPower = 0;
void stepFlywheel(){
  double position = getFlywheelPosition();
  double flywheelPower = flywheelController.step(position) * 127;
  if(flyTarget == FLY_IDLE) flywheelPower = 0;
  else if(flywheelPower < flyMin) flywheelPower = flyMin;
  moveFlywheel(flywheelPower);
}

double getFlywheelPower(){
  return flywheelPower;
}

void toggleFork(){
  if(forkController.getTarget() == 0){
      forkController.setTarget(FORK_STEP);
  }
  else{
      forkController.setTarget(0);
  }
}

void stepFork(){
  double position = fork_mtr.get_position();
  double power = forkController.step(position) * 127;
  fork_mtr.move(power);
}
