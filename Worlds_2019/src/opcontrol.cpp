#include "main.h"

/**
 *
 * MSU VEX 2018-19
 * Jacob Swaneck
 *
 * Runs the operator control code. This function will be started in its own task
 * with the default priority and stack size whenever the robot is enabled via
 * the Field Management System or the VEX Competition Switch in the operator
 * control mode.
 *
 * If no competition control is connected, this function will run immediately
 * following initialize().
 *
 * If the robot is disabled or communications is lost, the
 * operator control task will be stopped. Re-enabling the robot will restart the
 * task, not resume it from where it left off.
 */


#define JOY_DEADZONE 5
#define LIFT_MANUAL_SPEED 80
#define ADJUSTMENT_PER_TICK 5

void displayOpControlScreenOG();

void opcontrol() {
	op_control = true;
	idleFlywheel();

	//TOGGLES
	bool flywheelTogglePressed = false;
	bool reverseTogglePressed = false;
	bool reverseDrive = false;
	bool flywheelWasSettled = false;

	bool forkTogglePressed = false;

	bool hasLiftTarget = false;
	int liftTarget = 0;

	bool liftLowPressed = false;
	bool liftMedPressed = false;
	bool liftHighPressed = false;

	/** LOOP **/

	while (true) {
		if(op_control){
 	  pros::lcd::print(6, "Lift Target: %f", liftController.getTarget());

		//	Drive toggle

		if(master.get_digital(FLIP_DRIVE_BTN) && !reverseTogglePressed){
				reverseTogglePressed = true;
				reverseDrive = !reverseDrive;
		}
		else if(reverseTogglePressed && !master.get_digital(FLIP_DRIVE_BTN)){
				reverseTogglePressed = false;
		}


		// Drive

 		int left_drive_pow = master.get_analog(ANALOG_LEFT_Y);
 		int right_drive_pow = master.get_analog(ANALOG_RIGHT_Y);

		if(reverseDrive){
			int temp = left_drive_pow;
			left_drive_pow = -right_drive_pow;
			right_drive_pow = -temp;
		}

 		if(abs(left_drive_pow) < JOY_DEADZONE)
 			left_drive_pow = 0;
 		if(abs(right_drive_pow) < JOY_DEADZONE)
 			right_drive_pow = 0;

 		drive_left_1.move(left_drive_pow);
 		drive_left_2.move(left_drive_pow);
 		drive_left_3.move(left_drive_pow);
 		drive_right_1.move(right_drive_pow);
 		drive_right_2.move(right_drive_pow);
 		drive_right_3.move(right_drive_pow);


		//Conveyor and Intake
		int intake_pow = 0;
		int conveyor_pow = 0;

		if(master.get_digital(CONVEYOR_UP_BTN) && !master.get_digital(CONVEYOR_DOWN_BTN)){
				conveyor_pow = 127;
				intake_pow = 127;
			} else if(!master.get_digital(CONVEYOR_UP_BTN) && master.get_digital(CONVEYOR_DOWN_BTN)){
				conveyor_pow = -127;
		}
		moveConveyor(conveyor_pow);

		if(master.get_digital(INTAKE_IN_BTN) && !master.get_digital(INTAKE_OUT_BTN)){
				intake_pow = 127;
		} else if(!master.get_digital(INTAKE_IN_BTN) && master.get_digital(INTAKE_OUT_BTN)){
				intake_pow = -127;
		}
		intake_mtr.move(intake_pow);





 		//Lift

 		int lift_pow = 0;
		bool liftUp = master.get_digital(LIFT_UP_BTN);
		bool liftDown = master.get_digital(LIFT_DOWN_BTN);

		if(liftUp && liftDown){
			hasLiftTarget = false;
			if(master.get_digital(LIFT_LOW_BTN)){
				lift_pow = -80;
			}
			else if(master.get_digital(LIFT_HIGH_BTN)){
				lift_pow = 50;
			}

			if(master.get_digital(LIFT_MED_BTN)){
				resetLiftMotors();
			}
		}
 		else if(master.get_digital(LIFT_UP_BTN)){
				addLiftAdjustment(ADJUSTMENT_PER_TICK);
				hasLiftTarget = true;
		}
 		else if(master.get_digital(LIFT_DOWN_BTN)){
				addLiftAdjustment(-ADJUSTMENT_PER_TICK);
				hasLiftTarget = true;
		}
		else if(master.get_digital(LIFT_LOW_BTN) && !liftLowPressed){
				hasLiftTarget = true;
				targetLift(LIFT_LOW_CONST, true);
		}
		else if(master.get_digital(LIFT_MED_BTN) && !liftMedPressed){
				hasLiftTarget = true;
				targetLift(LIFT_MED_CONST, true);
		}
		else if(master.get_digital(LIFT_HIGH_BTN) && !liftHighPressed){
				hasLiftTarget = true;
				targetLift(LIFT_HIGH_CONST, true);
		}

		liftLowPressed = master.get_digital(LIFT_LOW_BTN);
		liftMedPressed = master.get_digital(LIFT_MED_BTN);
		liftHighPressed = master.get_digital(LIFT_HIGH_BTN);

		if(hasLiftTarget){
				stepLift();
		}
		else{
				moveLift(lift_pow);
		}

		//FLYWHEEL

		if(master.get_digital(FLYWHEEL_TOGGLE_BTN) && !flywheelTogglePressed){
			flywheelTogglePressed = true;
			toggleFlywheel();
		}
		else if(flywheelTogglePressed && !master.get_digital(FLYWHEEL_TOGGLE_BTN)){
			flywheelTogglePressed = false;
		}
		stepFlywheel();

		if(flywheelReady() && !flywheelWasSettled){
			flywheelWasSettled = true;
			master.rumble(".");
		}
		else if(flywheelWasSettled && !flywheelReady()){
			flywheelWasSettled = false;
		}

		// Fork
		if(master.get_digital(FORK_FLIP_BTN) && !forkTogglePressed){
				forkTogglePressed = true;
				toggleFork();
		}
		else if(forkTogglePressed && !master.get_digital(FORK_FLIP_BTN)){
				forkTogglePressed = false;
		}
		stepFork();
		//displayOpControlScreen();
		displayOpControlScreenOG();
		startAuton();
		}
		pros::delay(20);
	}
}


void displayOpControlScreenOG(){

	//pros::lcd::set_text(0, "Hello PROS User!");
}
