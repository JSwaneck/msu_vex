#include "main.h"

int selectedAuton = AUTON_RED;
bool auton_far = false;
bool auton_delay_switch = false;
bool auton_floor = true;
bool op_control = false;

void set_auton(int auton){
  pros::lcd::clear_line(7);
  if(auton == AUTON_RED){
    selectedAuton = auton;
    pros::lcd::print(7, "AUTON RED");
  }
  else if(auton == AUTON_BLUE){
    selectedAuton = auton;
    pros::lcd::print(7, "AUTON BLUE");
  }
  else
    selectedAuton = AUTON_NONE;
}
