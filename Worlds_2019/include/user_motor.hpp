#pragma once

#include "pros/motors.hpp"
#include "okapi/api/coreProsAPI.hpp"
#include "okapi/impl/chassis/controller/chassisControllerFactory.hpp"
#include "okapi/impl/control/iterative/iterativeControllerFactory.hpp"
#include "okapi/api/units/QLength.hpp"
#include "okapi/api/units/QAngle.hpp"
#include "okapi/impl/util/timer.hpp"


#define LIFT_LOW_CONST  0
#define LIFT_MED_CONST  1
#define LIFT_HIGH_CONST 2

using pros::Motor;
using pros::Controller;
using okapi::QLength;
using namespace okapi::literals;

extern Controller master;
extern Motor drive_left_1;
extern Motor drive_left_2;
extern Motor drive_left_3;
extern Motor drive_right_1;
extern Motor drive_right_2;
extern Motor drive_right_3;

extern Motor lift_left_bottom;
extern Motor lift_left_top;
extern Motor lift_right_bottom;
extern Motor lift_right_top;

extern Motor flywheel_1_mtr;
extern Motor flywheel_2_mtr;
extern Motor flywheel_3_mtr;

extern Motor intake_mtr;
extern Motor conveyor_mtr_1;
extern Motor conveyor_mtr_2;

extern Motor fork_mtr;


/** Chassis Controller **/

extern std::shared_ptr<okapi::ChassisControllerPID> chassisController;

/** Lift PID Controller **/

extern okapi::IterativePosPIDController liftController;
extern okapi::IterativePosPIDController liftCorrectionController;

/** Conveyor PID Controller **/

extern okapi::IterativePosPIDController conveyorCorrectionController;

/** Fork PID Controller **/

extern okapi::IterativePosPIDController forkController;

/** Launcher PID Controller **/

extern okapi::IterativeVelPIDController flywheelController;

/** Functions **/
extern int liftStartPointDegrees;
extern double liftAdjustments[];
extern int liftTarget;

extern int liftStartPointDegrees;
void setLiftStartPoint_degrees(QLength degrees);
double getLiftPosition_inches();
double getLiftPosition();
double getLiftPower();
double getLiftLeftPosition();
double getLiftRightPosition();
void moveLiftLeft(int power);
void moveLiftRight(int power);
void stepLift();
void moveLift(int power);
void targetLift(int targetConstant, bool toggleDrop = false);
int currentTargetConstant();
void updateTarget();
void addLiftAdjustment(double adjustment);
void setLiftAdjustment(double adjustment);

void moveConveyor(int power);
void enableConveyorPID();


void setFlywheelTarget(int target);
void stepFlywheel();
void idleFlywheel();
void toggleFlywheel();
void spinFlywheel();

bool flywheelReady();

void enableFlywheelPID();
double getFlywheelVelocity();
double getFlywheelPower();
double getFlywheelPosition();
void moveFlywheel(int power);
void setFlywheelMin(int min);

void disableAllControllers();
void resetLiftMotors();

void toggleFork();
void stepFork();



/** MISC **/
