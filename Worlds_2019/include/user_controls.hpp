#pragma once

#include "pros/misc.h"

#define INTAKE_IN_BTN       DIGITAL_R1
#define INTAKE_OUT_BTN      DIGITAL_R2

#define LIFT_UP_BTN         DIGITAL_L1
#define LIFT_DOWN_BTN       DIGITAL_L2

#define CONVEYOR_UP_BTN     DIGITAL_X
#define CONVEYOR_DOWN_BTN   DIGITAL_Y

#define FORK_FLIP_BTN       DIGITAL_A

#define FLIP_DRIVE_BTN      DIGITAL_RIGHT

#define FLYWHEEL_TOGGLE_BTN DIGITAL_B

#define LIFT_HIGH_BTN       DIGITAL_UP
#define LIFT_MED_BTN        DIGITAL_LEFT
#define LIFT_LOW_BTN        DIGITAL_DOWN
