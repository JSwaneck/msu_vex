# MSU VEX Robotics - PROS

This is a repo to store VEX Robotics Code for Jacob Swaneck's team. Currently holds code from the 2018-19 season

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

1. [PROS](https://pros.cs.purdue.edu/)
2. V5 Vex Cortex and robot (To run on)

### Installing

1. Download this repo onto your computer
2. Open project.pros
3. Build and Upload to robot!


## Credits

* **Jacob Swaneck** - *Vice President* - 2018/19 Season

Questions? Let me know!

swaneckj@msu.edu
